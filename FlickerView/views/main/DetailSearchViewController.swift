//
//  DetailSearchViewController.swift
//  FlickerView
//
//  Created by Mauker Olimpio on 4/26/17.
//  Copyright © 2017 Mauker Olimpio. All rights reserved.
//

import UIKit
import MapKit
import Kingfisher


class DetailSearchViewController: UIViewController, MKMapViewDelegate {

    var flickr:FlickrObject?
    
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var autorText: UILabel!
    @IBOutlet weak var descriptionText: UILabel!
    
    @IBOutlet weak var map: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = false
        // Do any additional setup after loading the view.
        
        if let flickr = flickr {
            flickr.getPhotoURL(for: .medium800, completion: { (url) in
                photo.kf.setImage(with: url, options: [.transition(.fade(0.2))])
            })
            self.titleText.text = flickr.json["title"].stringValue
            self.autorText.text = flickr.json["owner"].stringValue
            
            flickr.getLocation(completion: { (response) in
                
                print(response)
                
            })
        }
        
        map.delegate = self
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
