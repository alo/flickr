//
//  SearchViewController.swift
//  FlickerView
//
//  Created by Mauker Olimpio on 4/26/17.
//  Copyright © 2017 Mauker Olimpio. All rights reserved.
//

import UIKit
import FlickrKit
import SwiftyJSON
import JGProgressHUD
import Kingfisher


class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    
    var loading:JGProgressHUD?
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        FlickrManager.search(photoText: "dogs", maxResult: 5){

            self.loading?.dismiss()
            self.tableView.reloadData()
        }
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.navigationController?.isNavigationBarHidden = true
        
        self.searchBar.delegate = self
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        self.showLoading()
        FlickrManager.search(photoText: searchBar.text!, maxResult: 20) {
            self.tableView.reloadData()
            self.loading?.dismiss()
        }
    
    }
   

    func showLoading() {
        
        self.loading = JGProgressHUD(style: .dark)
        loading?.textLabel.text = "Loading"
        loading?.show(in: self.view)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return FlickrManager.photos.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let flickr = FlickrManager.photos[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell") as? SearchTableViewCell
        
        if let thumb = flickr.thumbURL {
            cell?.thumb.kf.setImage(with: thumb, options: [.transition(.fade(0.2))])
        }
        
        cell?.author.text = flickr.json["owner"].stringValue
        cell?.title.text = flickr.json["title"].stringValue
        
        return cell!
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destination = segue.destination as? DetailSearchViewController{
            
            if let selectedRow = self.tableView.indexPathForSelectedRow?.row{
                
                let flickr = FlickrManager.photos[selectedRow]
                destination.flickr = flickr
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
