//
//  FlickrManager.swift
//  FlickerView
//
//  Created by Mauker Olimpio on 4/26/17.
//  Copyright © 2017 Mauker Olimpio. All rights reserved.
//

import UIKit
import SwiftyJSON
import FlickrKit


class FlickrObject {
    var thumbURL:URL?
    var storedBigURL:URL?
    var bigURL:URL?{
        get{
            if let stored = self.storedBigURL{
                return stored
            }else{
                
                self.getPhotoURL(for: .medium640, completion: { (url) in
                    self.storedBigURL = url
                })
                return storedBigURL
            }
        }
    }
    var json:JSON
    init(json:JSON) {
        self.json = json
        
        getPhotoURL(for: .thumbnail100) { (url) in
            self.thumbURL = url
        }

    }
    
    func getPhotoURL(for:FKPhotoSize, completion:(URL)->Void){
        let photoURL = FlickrKit.shared().photoURL(for: .thumbnail100,
                                                   photoID: self.json["id"].stringValue,
                                                   server: self.json["server"].stringValue,
                                                   secret: self.json["secret"].stringValue,
                                                   farm: self.json["farm"].stringValue)
        
        completion(photoURL)
    }
    
    
    

    func getLocation(completion:@escaping (JSON)->Void){
        
            FlickrKit.shared().call("flickr.photos.geo.getLocation", args: ["api_key" : "d5ca4bcb7459c75e2efe58eff5bb1503", "photo_id":"\(self.json["id"].stringValue)"]) { (response, error) in
                
                DispatchQueue.main.async{
                    
                    if let response = response {
                        let json = JSON(response)
                        completion(json)
                    }
                    
                }
                
            }
    }
}

class FlickrManager: NSObject {

    static var photos = [FlickrObject]()
    
    static func search(photoText:String, maxResult:Int, callback:@escaping ()->Void) {
        
        FlickrManager.photos = [FlickrObject]()
        
        FlickrKit.shared().call("flickr.photos.search", args: ["text" : photoText, "per_page":"\(maxResult)", "has_geo":"1"]) { (response, error) in
            
            DispatchQueue.main.async{
                
                if let response = response {
                    
                    let json = JSON(response)
                    let photos = json["photos"]["photo"]
                    for (_,subJson):(String, JSON)  in photos{
                        FlickrManager.photos.append(FlickrObject(json: subJson ))
                    }
                }
                callback()
            }
            
        }
        
    }
    
}
